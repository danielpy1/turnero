from django.db import models

# Create your models here.
class Cliente(models.Model):
    name = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    ci = models.IntegerField(unique=True)
    phone = models.CharField(max_length=10)

    def __str__(self):
        return self.name + " " + self.lastname


class Servicio(models.Model):
    name = models.CharField(max_length=50)
    cost = models.IntegerField()

    def __str__(self):
        return self.name

class Prioridad(models.Model):
    description=models.CharField(max_length=50)
    priority=models.IntegerField()

    def __str__(self):
        return self.description
    
class Ticket(models.Model):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    prioridad = models.ForeignKey(Prioridad, on_delete=models.CASCADE)
    state = models.CharField(max_length=10)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(vars(self))