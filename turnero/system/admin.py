from django.contrib import admin
from .models import Cliente
from .models import Servicio
from .models import Prioridad

# Register your models here.
admin.site.register(Cliente)
admin.site.register(Servicio)
admin.site.register(Prioridad)
