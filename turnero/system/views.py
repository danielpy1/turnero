from django.shortcuts import render, redirect
from .models import Cliente,Prioridad,Servicio,Ticket
from .forms import ClienteForm,ClienteFormDisabled
from queue import PriorityQueue
from django.http import JsonResponse

def index(request):
    prioridades = Prioridad.objects.all()
    servicios = Servicio.objects.all()
    tickets = Ticket.objects.filter(state__exact='PENDIENTE')
    queue = PriorityQueue()
    cola=[]
    mensaje_modal = request.session.get('mensaje_modal')

    class Informacion_ticket:
        def __init__ (self,nro_ticket,cliente,prioridad,servicio,time):
            self.nro_ticket = nro_ticket
            self.cliente = cliente
            self.prioridad = prioridad
            self.servicio = servicio
            self.time = time

    for ticket in tickets:
        queue.put((ticket.prioridad.priority,ticket.creation_date,ticket))

    while not queue.empty():
        priority, creation_date, item = queue.get()
        cola.append(Informacion_ticket(item.id,item.cliente.name+" "+item.cliente.lastname,priority,item.servicio.name,creation_date.strftime("%H:%M:%S %d/%m/%Y")))

    if request.method == 'GET':
        return render(request, 'index.html', {'cola': cola , 'mensaje_modal': mensaje_modal})
    
    if request.method == 'POST':
        CI = request.POST.get('ci')
        try:
            cliente = Cliente.objects.get(ci__exact=CI)
            if cliente:
                form = ClienteFormDisabled()
                form.fields['name'].initial = cliente.name
                form.fields['lastname'].initial = cliente.lastname
                form.fields['ci'].initial = cliente.ci
                form.fields['phone'].initial = cliente.phone
                return render(request, 'index.html', {'cliente': cliente, 'ci': CI, 'prioridades': prioridades, 'servicios': servicios, 'form': form, 'cola': cola})
        except Cliente.DoesNotExist:
            form = ClienteForm()
            print("Cliente no existe")
            return render(request, 'index.html', {'form': form,'ci': CI, 'cola': cola})

def add_cliente(request):
    if request.method == 'POST':
        form = ClienteForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                request.session['mensaje_modal'] = 'Cliente agregado con éxito'
                print("Cliente agregado con éxito")
            except Exception as e:
                request.session['mensaje_modal'] = 'Error al agregar cliente, mire el log para más información'
                print("Exception: ",e)
            finally:
                return redirect('index')


def add_ticket(request):
    if request.method == 'POST':
        try:
            ticket=Ticket()
            ticket.cliente=Cliente.objects.get(id=request.POST.get('id_cliente'))
            ticket.servicio=Servicio.objects.get(id=request.POST.get('id_servicio'))
            ticket.prioridad=Prioridad.objects.get(id=request.POST.get('id_prioridad'))
            ticket.state='PENDIENTE'
            ticket.save()
            request.session['mensaje_modal'] = 'Ticket creado con éxito'
            print("Ticket creado con éxito")
        except Exception as e:
            request.session['mensaje_modal'] = 'Error al crear ticket, mire el log para más información'
            print("Exception: ",e)
        finally:
            return redirect('index')
        
def pop_cola(request):
    try:
        tickets = Ticket.objects.filter(state__exact='PENDIENTE')
        if(tickets):
            queue = PriorityQueue()
            for ticket in tickets:
                queue.put((ticket.prioridad.priority,ticket.creation_date,ticket))
            item = queue.get()[2]
            print("Ticket atendido: ",item)
            item.state='ATENDIDO'
            item.save()
            request.session['mensaje_modal'] = 'Ticket terminado con éxito\n item id: '+str(item.id)+' - cliente: '+str(item.cliente.name)+' '+str(item.cliente.lastname)+' - servicio: '+str(item.servicio.name)+' - prioridad: '+str(item.prioridad.priority)+' - hora: '+str(item.creation_date.strftime("%H:%M:%S %d/%m/%Y"))
            print("Ticket terminado con éxito")
        else:
            request.session['mensaje_modal'] = 'Cola vacía no hay turnos para eliminar'
    except Exception as e:
        request.session['mensaje_modal'] = 'Error al eliminar ticket, mire el log para más información'
        print("Exception: ",e)
    finally:
        return redirect('index')
        

def eliminar_mensaje_modal(request):
    if request.method == 'GET':
        try:
            del request.session['mensaje_modal']
            return JsonResponse({'mensaje': 'Variable eliminada correctamente'}, status=200)
        except KeyError:
            return JsonResponse({'mensaje': 'Variable no existe'}, status=200)
        


