from django import forms
from .models import Cliente

class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ('name', 'lastname', 'ci', 'phone')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'lastname': forms.TextInput(attrs={'class': 'form-control'}),
            'ci': forms.TextInput(attrs={'class': 'form-control','type': 'number','min':'0'}),
            'phone': forms.TextInput(attrs={'class': 'form-control','type': 'number','min':'0'})
        }

class ClienteFormDisabled(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ('name', 'lastname', 'ci', 'phone')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'disabled': 'disabled'}),
            'lastname': forms.TextInput(attrs={'class': 'form-control', 'disabled': 'disabled'}),
            'ci': forms.TextInput(attrs={'class': 'form-control', 'disabled': 'disabled','type': 'number','min':'0'}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'disabled': 'disabled','type': 'number','min':'0'})
        }
    
