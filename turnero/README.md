Pasos para levantar el proyecto:

#1- Entrar al repositorio y clonar el proyecto (https://gitlab.com/danielpy1/turnero) : 

	git clone git@gitlab.com:danielpy1/turnero.git

#2- Librerias librerias necesarias: 
	
Ubicarse en la raiz del proyecto y ejecutar el siguiente comando:
		
	pip install -r requirements.txt	

#3- Base de datos

Crear la base datos en PostreSQL:

Usar las siguientes credenciales ya que de esa forma está configurar el archivo settings:

	'NAME': 'turnero',
	'USER': 'postgres',
	'PASSWORD': 'postgres',
	'HOST': 'localhost',
	'PORT': '5432',

#4- Crear un superuser para configurar el sitio y administrar los servicios y las prioridades:

Ubicarse en la raiz del proyecto y ejecutar el siguiente comando:

	python manage.py createsuperuser

#5- Insertar las prioridades
	
	Descripcion: 3ra Edad - ALTA; Prioridad: 1
	Descripcion: Embarazadas - ALTA; Prioridad: 1
	Descripcion: Discapacidad - ALTA; Prioridad: 1
	Descripcion: Clientes corporativos - MEDIA; Prioridad: 2
	Descripcion: Clientes personales - BAJA; Prioridad: BAJA: 3

#6- Insertar los servicios

	Lista de servicios:
	1. Corte de cabello unisex; 10000
	2. Perfilado de cejas; 5000
	3. Lavado, secado y peinado; 15000
	4. Tinte de cabello; 20000
